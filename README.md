# CHS Commentaries and Community GraphQL Server 

[![pipeline status](http://gitlab.archimedes.digital/archimedes/ahcip-api/badges/develop/pipeline.svg)](http://gitlab.archimedes.digital/archimedes/ahcip-api/commits/develop)
[![coverage report](http://gitlab.archimedes.digital/archimedes/ahcip-api/badges/develop/coverage.svg)](http://gitlab.archimedes.digital/archimedes/ahcip-api/commits/develop)



User Generated Content
--
This feature allows AHCIP user to create their own Text Content to overwrite Text Content loaded from Text Server. 

These user generated Text Content combined with original Text from Text Server becomes edition/version variations of the original works from Text Server

examples:

```
# query user generated text with filter
{
  userTextMany(filter: {
    urn: "urn:cts:greekLit:tlg0012.tlg001.chs-venetusA:2.1", 
    status: public, 
    userID: "BkAsz9hUV"
  }) {
    urn
    content
    status
    userID
  }
}

```

```
# create user generated text in place of URN with version/edition
mutation{
  userTextCreate(
    userText:{
      urn:"urn:cts:greekLit:tlg0012.tlg001.chs-venetusA:2.1"
    	content:"ἄλλοι μέν ῥα θεοί τε καὶ ἀνέρες ἱπποκορυσταὶ"
    	status: public
    } 
  ) {
    urn
    content
    status
    userID
  } 
}
```
