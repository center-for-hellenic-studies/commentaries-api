import { schemaComposer } from 'graphql-compose';
import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM


// queries and mutations from types
import * as userTextTypes from './userText';
import * as groupTypes from './group';


const composedQueries = {
	...userTextTypes.Queries,
	...groupTypes.Queries,
};


const composedMutations = {
	...userTextTypes.Mutations,
	...groupTypes.Mutations,
};


const composedSchema = {
	composedQueries, 
	composedMutations
};


// build the final schema
const composeSchema = (queries, mutations) => {

	// put queries and mutations together
	schemaComposer.Query.addFields(queries);
	schemaComposer.Mutation.addFields(mutations);

	// generate GraphQLSchema
	return schemaComposer.buildSchema();
};


// exports
export { 
	composeSchema, 
	composedSchema, 
	composeWithMongoose
};
