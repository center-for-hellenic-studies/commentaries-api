import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { formatError } from 'apollo-errors';
import { GraphQLSchema, execute, subscribe } from 'graphql';
import {
	makeRemoteExecutableSchema,
	mergeSchemas,
	introspectSchema,
	transformSchema,
	RenameTypes,
	RenameRootFields
} from 'graphql-tools';
import { createApolloFetch } from 'apollo-fetch';
import fetch from 'node-fetch';
import { maskErrors } from 'graphql-errors';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import jwt from 'jsonwebtoken';
import httpContext from 'express-http-context';

import logger from './lib/logger';

// authentication
import { jwtAuthenticate } from './authentication';

// root query and mutation
import RootQuery from './graphql/queries/rootQuery';
import RootMutation from './graphql/mutations/rootMutation';

// schema
import * as schemaBuilder from './schema';

/**
 * Create a remote schema for merging with local schema definition
 * via example Schema stitching from the repo mentioned here:
 * https://dev-blog.apollodata.com/graphql-schema-stitching-8af23354ac37
 */
const createRemoteSchema = async (uri) => {
	const fetcher = async ({ query, variables, operationName, context }) => {
		const fetchResult = await fetch(uri, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ query, variables, operationName })
		});
		return fetchResult.json();
	};

	return makeRemoteExecutableSchema({
		schema: await introspectSchema(fetcher),
		fetcher
	});
};

const createRemoteSchemas = async () => {
	const remoteSchemaList = [];

	// validate link to Textserver
	const textserverURI = process.env.TEXTSERVER_URL || 'http://text.chs.orphe.us/graphql';
	const chsTextserverSchema = await createRemoteSchema(textserverURI)
		.catch((e) => {
			logger.error(`Could not link remote schema ${textserverURI}`);
			logger.error(e);
			return false;
		});
	logger.info(`Schema stitched ... ${textserverURI}`);
	remoteSchemaList.push(chsTextserverSchema);

	// validate link to CHSWeb-API
	const chswebAPIURI = process.env.CHSWEBAPI_URL || 'http://chsweb-api.chs.orphe.us/graphql';
	const chswebAPISchema = await createRemoteSchema(chswebAPIURI)
		.catch((e) => {
			logger.error(`Could not link remote schema ${chswebAPIURI}`);
			logger.error(e);
			return false;
		});
	if (chswebAPISchema) {
		const transformedChswebAPISchema = transformSchema(chswebAPISchema, [
			new RenameTypes(typeName => `CHS_${typeName}`),
			new RenameRootFields((op, fieldName) => `CHS_${fieldName}`),
		]);
		logger.info(`Schema stitched ... ${chswebAPIURI}`);
		remoteSchemaList.push(transformedChswebAPISchema);
	}

	// validate link to orpheus
	const orpheusAPIURI = process.env.ORPHEUS_API_URL || 'https://api.orphe.us/graphql';
	const orpheusAPISchema = await createRemoteSchema(orpheusAPIURI)
		.catch((e) => {
			logger.error(`Could not link remote schema ${orpheusAPIURI}`);
			logger.error(e);
			return false;
		});
	if (orpheusAPISchema) {
		const transformedOrpheusAPISchema = transformSchema(orpheusAPISchema, [
			new RenameTypes(typeName => `ORPHEUS_${typeName}`),
			new RenameRootFields((op, fieldName) => `ORPHEUS_${fieldName}`),
		]);
		logger.info(`Schema stitched ... ${orpheusAPIURI}`);
		remoteSchemaList.push(transformedOrpheusAPISchema);
	}

	return remoteSchemaList.filter(remoteSchema => remoteSchema);
};

/**
 * Root schema
 * @type {GraphQLSchema}
 */
const RootSchema = new GraphQLSchema({
	query: RootQuery,
	mutation: RootMutation,
});

// mask error messages for production
if (process.env.NODE_ENV === 'production') {
	maskErrors(RootSchema);
}

/**
 * For each graphql request add token from authorization headers
 */
const getGraphQLContext = (req) => {
	let token;

	if ('authorization' in req.headers) {
		token = req.headers.authorization.replace('JWT ', '');
		httpContext.set('token', token);
	}

	return {
		token,
	};
};

/**
 * Set up the graphQL HTTP endpoint
 * @param  {Object} app 	express app instance
 */
const setupGraphQL = async (app) => {

	let schema = RootSchema;

	// composed schema
	const composedSchema = schemaBuilder.composeSchema(
		schemaBuilder.composedSchema.composedQueries, 
		schemaBuilder.composedSchema.composedMutations,
	);
	schema = mergeSchemas({
		schemas: [schema, composedSchema],
	});

	// conditional schema stitching
	if (process.env.NODE_ENV !== 'test') {
		const validRemoteSchemaList = await createRemoteSchemas();
		if (validRemoteSchemaList.length > 0) {
			schema = mergeSchemas({
				schemas: [schema, ...validRemoteSchemaList],
			});
		}
	}

	// no null validation rules
	const validationRules = [
		(context) => {
			context._ast.definitions.forEach((definition) => {
				definition.variableDefinitions = definition.variableDefinitions || [];
			});
			return true;
		}
	];

	app.use('/graphql', jwtAuthenticate, graphqlExpress(req => ({
		schema,
		context: getGraphQLContext(req),
		formatError,
		validationRules,
	})));

	app.use('/graphiql', graphiqlExpress({
		endpointURL: '/graphql',
	}));
};

export default setupGraphQL;
