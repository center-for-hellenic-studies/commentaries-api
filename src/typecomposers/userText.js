import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import UserText from '../models/userText';

import UserTC from '../typecomposers/user';

import getUserID from '../lib/getUserID';

// build Type Composer with model and options
const customizationOptions = {};
const UserTextTC = composeWithMongoose(UserText, customizationOptions);
const UserTextITC = UserTextTC.getITC();

UserTextTC.addRelation('user', {
	resolver: () => UserTC.getResolver('findById'),
	prepareArgs: {
		_id: source => source.userID,
	},
	projection: { userID: true },
});


//
// custom resolvers
//
UserTextTC.addResolver({
	kind: 'mutation',
	name: 'createOneWithUserID',
	args: {
		userText: UserTextITC,
	},
	type: UserTextTC,
	resolve: async ({ args }) => {
		const userTextInstance = new UserText(args.userText);
		userTextInstance.userID = getUserID();
		const res = await userTextInstance.save();
		return res;
	},
});


// exports
export default UserTextTC;

