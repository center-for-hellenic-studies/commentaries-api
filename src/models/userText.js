import mongoose from 'mongoose';
import shortid from 'shortid';


const UserTextModel = new mongoose.Schema({
	_id: {
		type: String,
		default: shortid.generate
	},

	urn: {
		type: String,
		index: true,
		description: 'can be CTS URN (eg. urn:cts:greekLit:tlg0012.tlg001:2.1) or full URN with edition info (eg.urn:cts:greekLit:tlg0012.tlg001.perseus-grc2)',
	},

	content: {
		type: String,
		description: 'user generated content that replaces content from text server for the textNode of that URN',
	},

	status: {
		type: String,
		optional: true,
		default: 'private',
		enum: ['public', 'private'],
	},

	userID: {
		type: String,
		required: true,
		ref: 'Users'
	}
});


const UserText = mongoose.model('UserText', UserTextModel);

export default UserText;

