import getUserID from './getUserID';
import UserText from '../models/userText';
import Group from '../models/group';


// authorization functions
const userAdminsGroup = async (userID, groupID) => {
	const group = await Group.findById(groupID);
	return group.admins.includes(userID);
};

const userOwnsUserText = async (userID, userTextID) => {
	const userText = await UserText.findById(userTextID);
	return userID === userText.userID;
};

// resolver to authorization function mapper
const hasAccess = async (rule, rp) => {

	let authorized = false;
	const userID = getUserID();

	switch (rule) {
	case 'userIDExist':
		authorized = userID;
		break;

	case 'userOwnsGroup':
		authorized = userID && await userAdminsGroup(userID, rp.args.record._id);
		break;

	case 'userOwnsUserText':
		authorized = userID && await userOwnsUserText(userID, rp.args.record._id);
		break;

	default:
		authorized = false;
		break;
	}

	return authorized;
};

// resolver wrapper
const authorize = (rule, resolvers) => {
	
	Object.keys(resolvers).forEach((k) => {
		resolvers[k] = resolvers[k].wrapResolve(next => async (resolveParams) => {

			// get authorization results
			if (await hasAccess(rule, resolveParams)) {
				return next(resolveParams);
			}

			// all authorization checks failed
			throw new Error('User is not authorized for this action.');
		});
	});
	return resolvers;
};


export default authorize;
